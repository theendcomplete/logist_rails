class CreateTransportCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :transport_companies do |t|
      t.text :address
      t.text :comment
      t.string :name
      t.string :phone

      t.timestamps
    end
  end
end
