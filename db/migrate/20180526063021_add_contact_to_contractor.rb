class AddContactToContractor < ActiveRecord::Migration[5.1]
  def change
    add_column :contractors, :contact, :reference
  end
end
