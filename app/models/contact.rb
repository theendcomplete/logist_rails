class Contact < ApplicationRecord
  belongs_to :contractor
  validates :name, presence: true

  rails_admin do
    configure :contractor do
      label 'Контрагент: '
    end
  end
end
