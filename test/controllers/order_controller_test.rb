require 'test_helper'

class OrderControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get order_new_url
    assert_response :success
  end

  test "should get list" do
    get order_list_url
    assert_response :success
  end

  test "should get delete" do
    get order_delete_url
    assert_response :success
  end

  test "should get update" do
    get order_update_url
    assert_response :success
  end

end
