class WelcomeController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  def index
    @order = Order.new
  end

  def about
    redirect_to "/orders/new"
    # @order = Order.new
  end
end
