class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :phone
      t.text :comment
      t.belongs_to :contractor, foreign_key: true

      t.timestamps
    end
  end
end
