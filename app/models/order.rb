class Order < ApplicationRecord
  belongs_to :user
  has_many :adv_options
  has_many :purposes
  # geocoded_by :full_street_address   # can also be an IP address
  # after_validation :geocode          # auto-fetch coordinates
  #
  #Mounts paperclip image
  has_attached_file :file

  #This validates the type of file uploaded. According to this, only images are allowed.
  # validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates_attachment :file, :content_type => {:content_type => %w(image/jpeg image/jpg image/png application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document)},
                       size: {in: 0..5.megabytes}

end
