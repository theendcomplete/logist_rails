# Be sure to restart your server when you modify this file.

# ActiveSupport::Reloader.to_prepare do
#   ApplicationController.renderer.defaults.merge!(
#     http_host: 'example.org',
#     https: false
#   )
# end
#   protect_from_forgery with: :exception
#   before_action :authenticate_user!
#
#
# protect_from_forgery with: :exception
# before_action :authenticate_user!

def configure_permitted_parameters
  devise_parameter_sanitizer.for(:sign_in) {|u| u.permit(:username, :password, :remember_me)}
  devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:username, :first_name, :last_name, :password, :password_confirmation, :email)}
end