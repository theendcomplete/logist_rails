class CreateDrivers < ActiveRecord::Migration[5.1]
  def change
    create_table :drivers do |t|
      t.string :name
      t.references :car
      t.timestamps
    end

    # create_table :driver_appointments do |t|
    #   t.belongs_to :cars, index: true
    #   t.belongs_to :drivers, index: true
    #   t.datetime :appointment_date
    #   t.timestamps
    # end
    # add_reference :drivers, :car, foreign_key: true
  end
end
