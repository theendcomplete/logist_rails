# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
ActionMailer::Base.smtp_settings = {

    :address => 'smtp.yandex.ru',

    :port => '587',

    :authentication => :plain,

    :user_name => ENV['LOGIST_EMAIL'],

    :password => ENV['LOGIST_PASSWORD'],

    :domain => ENV['LOGIST_DOMAIN'],

    :enable_starttls_auto => true

}
