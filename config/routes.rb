Rails.application.routes.draw do

  root 'welcome#index'

  resources :orders

  resources :locations


  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :contacts
  resources :contractors
  resources :cars
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
