class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :address
      t.text :comment
      t.datetime :start_date
      t.datetime :end_date
      t.datetime :end_date
      t.datetime :work_date
      # t.belongs_to :user, foreign_key: true
      t.references :user, foreign_key: true
      t.string :sum
      t.boolean :dover
      t.boolean :parking
      t.boolean :heat
      t.boolean :wepay
      t.boolean :big
      t.references :contractor
      t.references :driver
      t.references :car
      t.timestamps
    end
    # add_reference :orders, :contractor, foreign_key: true
    # add_reference :orders, :driver, foreign_key: true
    # add_reference :orders, :car, foreign_key: true
    # t.has_one :contractor
    # t.has_one :driver
    # t.has_one :car
  end
end
