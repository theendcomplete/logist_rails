class AddAttachmentFileToOrders < ActiveRecord::Migration[5.1]
  def self.up
    change_table :orders do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :orders, :file
  end
end
